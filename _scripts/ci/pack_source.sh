#!/bin/bash
#
# This file is part of SystemTestPortal.
# Copyright (C) 2018  Institute of Software Technology, University of Stuttgart
#
# SystemTestPortal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SystemTestPortal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
#

# test if current commit is tagged then we have a release build
if git describe --exact-match --tags HEAD ; then
    mkdir /builds/stp-team/systemtestportal-webapp/source/
    tar -cvzf /builds/stp-team/systemtestportal-webapp/source/${FILE_NAME} .
    echo "$STP_GPG_PRIVATE" > stp-private.key
    gpg --allow-secret-key-import --import stp-private.key
    cd /builds/stp-team/systemtestportal-webapp/source
    gpg --detach-sign /builds/stp-team/systemtestportal-webapp/source/${FILE_NAME}
    ls -l
fi