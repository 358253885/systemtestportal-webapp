/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package modal

//Contains all data for an delete confirmation modal
type DeleteMessage struct {
	Header         string
	Message        string
	ModalID        string
	DeleteButtonID string
}

//Data for a delete confirmation modal of labels
var LabelDeleteMessage = DeleteMessage{
	Header:         " Delete Label",
	Message:        "Do you really want to delete the label? This is irreversible!",
	ModalID:        "deleteLabels",
	DeleteButtonID: "buttonDeleteLabelConfirm",
}

//Data for a delete conformation modal of Versions
var VersionDeleteMessage = DeleteMessage{
	Header:         " Delete Version",
	Message:        "Do you really want to delete the version? This is irreversible! ",
	ModalID:        "deleteVersions",
	DeleteButtonID: "buttonDeleteVersionConfirm",
}

//Data for a delete confirmation modal of test cases
var TestCaseDeleteMessage = DeleteMessage{
	Header:         " Delete Test Case",
	Message:        "Do you really want to delete the test case? This is irreversible!",
	ModalID:        "deleteTestCase",
	DeleteButtonID: "buttonDeleteTestCase",
}

//Data for a delete confirmation modal of test sequences
var TestSequenceDeleteMessage = DeleteMessage{
	Header:         " Delete Test Sequence",
	Message:        "Do you really want to delete the test sequence? This is irreversible!",
	ModalID:        "deleteTestSequence",
	DeleteButtonID: "buttonDeleteTestSequence",
}

//Data for a delete confirmation modal of projects
var ProjectDeleteMessage = DeleteMessage{
	Header:         " Delete Project",
	Message:        "Do you really want to delete the project? This is irreversible!",
	ModalID:        "deleteProject",
	DeleteButtonID: "buttonDeleteProject",
}
