{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "modal-testcase-selection"}}
<div class="modal fade" id="modal-testcase-selection" tabindex="-1" role="dialog" aria-labelledby="modal-testcase-selection-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-testcase-selection-label">{{T "Select test cases" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="testcase-selection">
                {{range .TestCases }}
                    <div class="form-check checkbox-list-element">
                        <input class="form-check-input testCaseSelector" type="checkbox" value="" id="{{ .Name }}" name="{{ .Name }}">
                        <label class="form-check-label" for="{{ .Name }}"> {{ .Name }} </label>

                        <!-- Alternative layout somehow prevents adding a test case a second time-->
                        {{/*<div class="custom-control custom-checkbox">*/}}
                            {{/*<input id="{{ .Name }}" type="checkbox" class="custom-control-input testCaseSelector" name="{{ .Name }}">*/}}
                            {{/*<label class="custom-control-label" for="{{ .Name }}">{{ .Name }}</label>*/}}
                        {{/*</div>*/}}
                    </div>
                {{end}}
                <span id="testCaseSelectorPlaceholder" class="text-muted">{{T "No Test Cases" .}}</span>
            </div>
            <div class="modal-footer">
                <button id="buttonAddTestCases" type="button" class="btn btn-primary">{{T "Add test cases" .}}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>

    <script src='/static/js/modal/select-testcase.js'></script>

</div>
{{end}}