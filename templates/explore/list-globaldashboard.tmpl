{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

<!-- Translated to German -->
{{define "content"}}
<!-- Content-->
<div class="row searchbar">
    <div class="col-12">
        <form role="search" class="search-form">
            <label class="sr-only">{{T "Search" .}}</label>
            <div class="input-group flex-nowrap">
                <input id="projects-search-input" type="search" name="s"
                       class="order-1 search-field form-control float-left rounded" placeholder="Search...">
            </div>
        </form>
    </div>
</div>
<div class="card">
    <div class="m-4">
        <table id="projects-list" class="table w-100" style="opacity: 0">

        {{ if .Projects}}
            <thead>
            <tr>
                <th class="pl-0 pt-0" style="border-top: none;">
                    <h5>
                    {{ if .SignedIn }}
                        {{ T "Projects" .}}
                    {{ else }}
                    {{ T "Public Projects" .}}
                    {{ end }}
                    </h5>
                </th>
                <th style="border-top:none">
                        <i class="fa fa-hashtag text-secondary" aria-hidden="true" data-toggle="tooltip"
                           data-placement="bottom" title="Total Tests"></i>
                </th>
                <th style="border-top:none">
                        <i class="fa fa-check-circle text-success" aria-hidden="true" data-toggle="tooltip"
                           data-placement="bottom" title="Passed">
                        </i>
                </th>
                <th style="border-top:none">
                        <i class="fa fa-exclamation-circle text-warning" aria-hidden="true" data-toggle="tooltip"
                           data-placement="bottom" title="Partially Successful">
                        </i>
                </th>
                <th style="border-top:none">
                        <i class="fa fa-exclamation-circle text-danger" aria-hidden="true" data-toggle="tooltip"
                           data-placement="bottom" title="Failed">
                        </i>
                </th>
                <th style="border-top:none">
                        <i class="fa fa-question-circle text-secondary" aria-hidden="true" data-toggle="tooltip"
                           data-placement="bottom" title="Not Assessed">
                        </i>
                </th>
                <th style="border-top:none">
                        <i class="fa fa-times-circle text-secondary" aria-hidden="true" data-toggle="tooltip"
                           data-placement="bottom" title="Not Yet Executed">
                        </i>
                </th>
            </tr>
            </thead>

            <tbody>
            {{range $key, $value := .Projects }}
            <tr class="projectLine" id="{{ $key.Name }}" title="{{ $key.Owner }}">
                <td class="pl-0">
                    <span class="filter-by-search">{{ $key.Owner.Actor }}/{{ $key.Name }}</span>
                </td>
                <td>
                    {{index $value 0}}
                </td>
                <td>
                    {{index $value 1}}
                </td>
                <td>
                    {{index $value 2}}
                </td>
                <td>
                    {{index $value 3}}
                </td>
                <td>
                    {{index $value 4}}
                </td>
                <td>
                    {{index $value 5}}
                </td>
            </tr>
            {{ end }}
            </tbody>

        {{ else }}
            <thead>
            <tr>
                <th class="pl-0 pt-0" style="border-top: none;">
                    <h5>
                    {{ if .SignedIn }}
                        {{ T "Show public, internal and your private projects" .}}
                    {{ else }}
                    {{ T "Show all public projects" .}}
                    {{ end }}
                    </h5>
                </th>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td>
                    <!-- Placeholder -->
                    <p class="text-center empty-project-list">
                        <span>
                        {{ T "This list contains all visible projects. Currently, there are no projects." .}}
                        </span><br/><br/>
                    {{ if .SignedIn }}
                        <a class="btn btn-primary align-middle" href="/projects/new">
                            <span>
                            {{ T "New Project" .}}
                            </span>
                        </a>
                    {{ end }}
                    </p>
                </td>
            </tr>
            </tbody>

        {{ end }}

        </table>

    </div>
</div>


<script src="/static/js/explore/explore.js" integrity="{{sha256 "/static/js/explore/explore.js"}}"></script>
<script type='text/javascript'>

    $(".projectLine").on("click", event => useProjectLink(event));
    $("time.timeago").timeago();

    $(() => {
        $("#menuExploreGlobalDashboard").addClass("disabled active");
        $("#navbarExploreGlobalDashboard").addClass("disabled active");

        const idTable = "projects-list";
        const idSearch = "projects-search-input";

        const listElementsPerPage = 10;
        const listElements = "projects";

        initializeDataTable(idTable, idSearch, listElementsPerPage, listElements)

    });

</script>
{{end}}

