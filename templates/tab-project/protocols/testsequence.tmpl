{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
<div class="modal fade" id="helpModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{T "Test Case Protocol Details" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="help-table">
                    <tr>
                        <td colspan="3">
                            {{T "This shows the details of the execution of a test sequence" .}}.
                        </td>
                    </tr>
                    <tr class="h5">
                        <th>{{T "Buttons" .}}</th>
                        <th>{{T "Function" .}}</th>
                        <th><span class="d-none d-sm-inline">{{T "Shortcut" .}}</span></th>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-secondary">
                                <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Back" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Get back to the test sequence protocol list" .}}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>{{T "Backspace" .}}</button></td>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-primary">
                                <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline">{{T "Export to PDF" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Export the test protocol to a PDF file and download it" .}}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>E</button></td>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-primary">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline">{{T "Export to Markdown" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Open a dialog containing the test protocol as markdown text" .}}.</td>
                        <td><span class="d-none d-sm-inline">-</span></td>
                    </tr>
                </table>
                <span class="mt-3 float-left">{{T "For more information visit our" .}} <a href="http://docs.systemtestportal.org" target="_blank">{{T "documentation" .}}</a>.</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>

<div class="tab-card card" id="tabTestProtocols">
    <nav class="navbar navbar-light action-bar p-3">
        <div class="input-group flex-nowrap">
            <button class="btn btn-secondary mr-2" id="buttonBack">
                <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                <span class="d-none d-sm-inline"> {{T "Back" .}}</span>
            </button>
            <button class="btn btn-primary mr-2" id="buttonPdf">
                <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                <span class="d-none d-sm-inline">{{T "Export to PDF" .}}</span>
            </button>
            <button class="btn btn-primary mr-2" id="buttonMd">
                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                <span class="d-none d-sm-inline">{{T "Export to Markdown" .}}</span>
            </button>
        </div>
    </nav>

    {{template "modal-protocol-md" .}} 
    
    <div class="row tab-side-bar-row">
        <div class="col-md-9 p-3">
            <h4 class="mb-3">
                <span id="contentTestSequenceResult" class="text-muted">
                    <i
                    {{ if eq .Protocol.Result 1 }}
                    class="fa fa-check-circle text-success" title="{{T "Passed" .}}"
                    {{ else if eq .Protocol.Result 2 }}
                    class="fa fa-info-circle text-warning" title="{{T "Partially Successful" .}}"
                    {{ else if eq .Protocol.Result 3 }}
                    class="fa fa-times-circle text-danger" title="{{T "Failed" .}}"
                    {{ else }}
                    class="fa fa-question-circle text-secondary" title="{{T "Not Assessed" .}}"
                    {{ end }}
                    aria-hidden="true" data-toggle="tooltip" data-placement="bottom">
                    </i>
                </span>
                <span id="protocolName">{{T "Protocol of" .}} {{ .TestSequence.Name }}</span>
            </h4>
            <div class="form-group">
                <label><strong>{{T "Test Sequence Description" .}}</strong></label>
                <p id="contentTestSequenceDescription" class="text-muted">
                {{ .TestSequenceVersion.Description }}
                </p>
            </div>
            <div class="form-group">
                <label><strong>{{T "Test Sequence Preconditions" .}}</strong></label>
                <ul id="contentPreconditions" class="list-group">
                {{ if eq (len .Protocol.PreconditionResults) 0 }}
                    <li class="list-group-item preconditionItem">
                        <span>{{T "No Preconditions" .}}</span>
                    </li>
                {{ end }}
                {{ range .Protocol.PreconditionResults }}
                    <li class="list-group-item preconditionItem">
                        <span>{{ .Precondition.Content }}</span>
                        <span><strong>({{ .Result }})</strong></span>
                    </li>
                {{ end }}
                </ul>
            </div>
            <div class="form-group">
                <label><strong>{{T "Test Case Results" .}}</strong></label>
            </div>
            <table class="table table-hover table-responsive">
                <thead>
                <tr>
                    <th style="width:10%;">{{T "Result" .}}</th>
                    <th style="width:90%;">{{T "Test Case" .}}</th>
                    <!--<th style="width:40%;">Time</th>-->
                </tr>
                </thead>
                <tbody id="protocolTable">
                    {{ range .CaseProtocols }}
                        <tr id="{{ .ProtocolNr }}" class="cursor-clickable"
                            onclick="ProtocolDataTable.onUserClickedRow({{ .TestVersion.Test }}, {{ .ProtocolNr }}, false)">
                            <td>
                                <i
                                    {{ if eq .Result 1 }}
                                        class="fa fa-check-circle text-success" title="{{T "Passed" .}}"
                                    {{ else if eq .Result 2 }}
                                        class="fa fa-info-circle text-warning" title="{{T "Partially Successful" .}}"
                                    {{ else if eq .Result 3 }}
                                        class="fa fa-times-circle text-danger" title="{{T "Failed" .}}"
                                    {{ else }}
                                        class="fa fa-question-circle text-secondary" title="{{T "Not Assessed" .}}"
                                    {{ end }}
                                        aria-hidden="true" data-toggle="tooltip" data-placement="bottom">
                                </i>
                            </td>
                            <td>
                                {{ .TestVersion.TestID.Test }}
                            </td>
                        </tr>
                    {{ end }}
                </tbody>
            </table>
        </div>
        <div class="col-md-3 p-3 tab-side-bar">
            <div class="form-row">
                <div class="col-12">
                    <strong>{{T "Execution Date" .}}</strong>
                </div>
                <div class="col-12">
                    <p id="contentTestSequenceExecutionDate" class="text-muted">
                        <time class="timeago">
                        {{ .Protocol.ExecutionDate }}
                        </time>
                    </p>
                </div>
                <div class="col-12">
                    <strong>{{T "Execution Time" .}}</strong>
                </div>
                <div {{if not .SystemSettings.IsExecutionTime}} hidden {{end}} class="col-12">
                    <p id="contentTestSequenceExecutionTime" class="text-muted">
                        <time class="timeago">
                        {{ .ExecutionTime }}
                        </time>
                    </p>
                </div>
                <div class="col-12" id="divTestCaseTester">
                    <strong>{{T "Tester" .}}</strong>
                </div>
            {{ if not .Protocol.IsAnonymous }}
                <div class="col-12">
                    <a href="/users/{{.Protocol.UserName}}"><img src="{{getImagePath (getUserImageFromName .Protocol.UserName) "user"}}" alt="" class="rounded-circle profile-picture" height="24" width="24"></a>
                    <a href="/users/{{.Protocol.UserName}}"><span class="text-muted assignmentAssignee">{{ .Protocol.UserName }}</span></a>
                </div>
            {{ else }}
                <div class="col-12">
                    <span class="text-muted">{{T "Anonymous" .}}</span>
                </div>
            {{ end }}
                <div class="col-12 mt-2">
                    <strong>{{T "System Version" .}}</strong>
                </div>
                <div class="col-12">
                    <p id="contentTestCaseSUTVersion" class="text-muted">
                    {{ .Protocol.SUTVersion }}
                    </p>
                </div>
                <div class="col-12">
                    <strong>{{T "System Variant" .}}</strong>
                </div>
                <div class="col-12">
                    <p id="contentTestCaseSUTVariant" class="text-muted">
                    {{ .Protocol.SUTVariant }}
                    </p>
                </div>
                <div class="col-12">
                    <strong>{{T "Test Sequence Version" .}}</strong>
                </div>
                <div class="col-12">
                    <a id="contentTestVersion" class="cursor-clickable"
                       href="/{{ .Project.Owner }}/{{ .Project.Name }}/testsequences/{{ .TestSequence.Name }}?version={{ .TestSequenceVersion.VersionNr }}"
                       onclick="openTestVersion(this, '/{{ .Project.Owner }}/{{ .Project.Name }}/testsequences/{{ .TestSequence.Name }}', {{ .TestSequenceVersion.VersionNr }})">{{ .Protocol.TestVersion.TestVersion }}</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Import Scripts here -->
<script src="/static/js/project/testprotocols.js" integrity="{{sha256 "/static/js/project/testprotocols.js"}}"></script>

<script>
    $("#helpIcon").removeClass("d-none");
    $("#printerIcon").addClass("d-none");

    assignListeners();
</script>
{{end}}

