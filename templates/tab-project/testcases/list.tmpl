{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
<div class="modal fade" id="helpModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{T "Test Case List" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="help-table">
                    <tr>
                        <td colspan="3">
                            <ul>
                                <li>{{T "Choose test cases to display them in detail." .}}</li>
                            {{ if or .Member .Admin }}
                                <li>{{T "Add a new test case (see button below)." .}}</li>{{ end }}
                            </ul>
                        </td>
                    </tr>
                    <tr class="h5">
                        <th>{{T "Buttons" .}}</th>
                        <th>{{T "Function" .}}</th>
                        <th><span class="d-none d-sm-inline">{{T "Shortcut" .}}</span></th>
                    </tr>
                    <tr>
                        <td>
                            <button class="order-3 btn btn-primary align-middle">
                                + <span class="d-none d-sm-inline">{{T "New Test Case" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Add a new test case." .}}</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>+</button></td>
                    </tr>
                </table>
                <span class="mt-3 float-left">{{T "For more information visit our" .}} <a href="http://docs.systemtestportal.org" target="_blank">{{T "documentation" .}}</a>.</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>

<div class="tab-card card" id="tabTestCases">


    <nav class="navbar navbar-light action-bar p-3 d-print-none">
        <label class="sr-only">Search</label>
        <div class="input-group flex-nowrap">
            <span>
                <input id="test-case-query-search" style="width: 100%;" type="search" name="s" class="order-1 search-field form-control float-left"
                       placeholder="{{T "Search..." .}}">
            </span>
        {{ if or .Member .Admin}}
            <button class="order-3 btn btn-primary align-middle ml-4 float-right" id="buttonNewTestCase">
                + <span class="d-none d-sm-inline">{{T "New Test Case" .}}</span>
            </button>
        {{ else }}
            <button class="order-3 btn btn-primary align-middle ml-4 float-right cursor-not-allowed disabled" type="button"
                    id="buttonNewTestCaseDisabled" data-toggle="tooltip" data-placement="bottom"
                    title="{{T "You have to be a member of the project to create a new test case" .}}">
                + <span class="d-none d-sm-inline">{{T "New Test Case" .}}</span>
            </button>
        {{ end }}

        </div>
    </nav>

    <div class="row tab-side-bar-row">
        <div class="col-md-9 pt-0 pb-3 pl-3 pr-3">

            <table id="table-test-cases" class="table w-100 d-none">

                <thead>
                <tr><th style="border-top: none;">
                    <h4> {{T "Test Cases" .}} <span id="labelFilterContainerText">({{T "All" .}})</span>
                        <span id="labelFilterContainer">
                            {{ range .Project.Labels}}
                                <span id ="list-header-label-{{ .Id }}"
                                      class="badge badge-primary filterBadge listHeaderBadge clickIcon list-container-label-{{ .Id }}"
                                      data-toggle="tooltip"
                                      data-original-title="{{ .Description }}"
                                      style="background-color: {{ .Color }};
                                              color: {{.TextColor}};
                                              display:none;">{{ .Name }}
                                    <strong>
                                        <span class="filterBadgeClose clickIcon" id="{{ .Id }}">&times;</span>
                                    </strong>
                                </span>
                            {{end}}
                        </span>
                    </h4>
                </th></tr>
                </thead>

                <tbody>
                {{ range .TestCases }}
                    <tr class="testCaseLine">
                        <td id="{{ .Name }}">
                        {{ .Name }}
                        {{ range .Labels }}
                            <span   id="list-test-label-{{ .Id }}"
                                    class="badge badge-primary filterBadge clickIcon list-container-label-{{ .Id }} list-list-label-{{ .Id }}"
                                    data-toggle="tooltip"
                                    data-original-title="{{ .Description }}"
                                    style="background-color: {{ .Color }};
                                            color: {{.TextColor}};
                                            float: right">{{ .Name }}</span>
                        {{ end }}
                        </td>
                    </tr>
                {{else}}
                <tr>
                    <td>
                        <p class="text-center empty-project-list">
                                    <span class="text-muted text-center">
                                        {{T "There are no test cases yet." .}} {{ if or .Member .Admin }} {{T "Do you want to create a new test case?" .}} {{ end }}
                                    </span>
                            <br/><br/>

                        {{ if or .Member .Admin }}
                            <button class="btn btn-primary align-middle"
                                    id="buttonFirstTestCase">+ {{T "New Test Case" .}}</button>
                        {{ end }}
                        </p>
                    </td>
                </tr>
                {{end}}
                </tbody>
            </table>

        </div>
        <div class="col-md-3 p-3 tab-side-bar d-print-none">
            <h4>Filters</h4>
            <h5>
                {{T "Labels" .}}
            {{ if or .Member .Admin }}
                <button type="button" class="btn btn-primary btn-sm mb-2" data-toggle="modal" data-target="#modal-manage-labels" >
                    <i class="fa fa-wrench" aria-hidden="true"></i>
                </button>
            {{ end }}
            </h5>
            <p id="listLabelContainer">
            {{ with .Project }}
                {{ range .Labels }}
                    <span   id="list-container-label-{{ .Id }}"
                            class="badge badge-primary clickIcon list-container-label-{{ .Id }}"
                            data-toggle="tooltip"
                            data-original-title="{{ .Description }}"
                            onclick="onListLabelClick({{ .Id }})"
                            style="background-color: {{ .Color }};
                                    color: {{.TextColor}}">{{ .Name }}</span>
                {{ end }}
                    <span id="labelContainerText" class="text-muted">{{T "No Labels" .}}</span>
            {{ end }}
            </p>
        </div>
    </div>
</div>

{{template "modal-manage-labels" . }}

<!-- Import Scripts here -->
<script src="/static/js/project/testcases.js" integrity="{{sha256 "/static/js/project/testcases.js"}}"></script>
<script src="/static/js/util/common.js" integrity="{{sha256 "/static/js/util/common.js"}}"></script>

<script>
    //Labels
    setListLabelText();

    assignButtonsTestCase();
    $("#printerIcon").removeClass("d-none");
    $("#helpIcon").removeClass("d-none");

    function printer() {
        window.open(currentURL().appendSegment('print').toString(),
                'newwindow',
                'width=600,height=800');
        return false;
    }

    $(() => {

        const table = $('#table-test-cases');
        const dataTable = table.DataTable({
            "sDom": '<"top">rt<"bottom"p><"clear">',

            "pageLength": 15,
            drawCallback: function() {
                const pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
                pagination.toggle(this.api().page.info().pages > 1);
            },

            "language": {
                "zeroRecords":     "No Test Cases found matching query ..."
            }
        });

        $("#test-case-query-search").on("keyup", function () {
            dataTable.search( this.value ).draw()
        });

        table.removeClass("d-none");
    })
</script>
{{end}}


