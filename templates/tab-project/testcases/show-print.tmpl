{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
<div id="tabTestCases">
    <div class="row tab-side-bar-row">
        <div class="col-md-12">
            {{ if ne .TestCaseVersion.VersionNr (index .TestCase.TestCaseVersions 0).VersionNr }}
            <div class="alert alert-warning">
                <strong>{{T "Warning" .}}!</strong> {{T "This is an older version of the test case" .}}
            </div>
            {{ end }}
            <h4 class="mb-3">{{ .Project.Owner }}/{{ .Project.Name }} : {{ .TestCase.Name }}</h4>
            {{ with .TestCaseVersion }}
            <div class="form-group">
                <label><strong>{{T "Test Case Description" .}}</strong></label>
                <p class="text-muted">
                    {{ with .Description }}
                    {{ . }}
                    {{ else }}
                    {{T "No Description" .}}
                    {{ end }}
                </p>
            </div>
            <div class="form-group">
                <label><strong>{{T "Test Case Preconditions" .}}</strong></label>
                <p class="text-muted">
                    {{ with .Preconditions }}
                    {{ . }}
                    {{ else }}
                    {{T "No Conditions" .}}
                    {{ end }}
                </p>
            </div>
            <div class="form-group">
                <label><strong>{{T "Test Steps" .}} ({{ len .Steps }})</strong></label>
                {{ if .Steps }}
                <table class="non-collapsed-table">
                    {{ range $index, $element := .Steps }}
                        <tr>
                            <td valign="top"><strong>{{ $index }}. </strong></td>
                            <td valign="top"  class="w-25">{{T "Action"}}: </td>
                            <td class="w-100 text-muted">{{ .Action }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td valign="top"  class="w-25">{{T "Expected Result" .}}: </td>
                            <td class="w-100 text-muted">{{ .ExpectedResult }}</td>
                        </tr>
                        <tr class="hide-form">
                            <td></td>
                            <td valign="top" class="w-25">{{T "Actual Result" .}}: </td>
                            <td class="w-100 fill-in-area fill-in-area-two-line"> </td>
                        </tr>
                        <tr class="hide-form">
                            <td></td>
                            <td valign="top" class="w-25">{{T "Notes" .}}: </td>
                            <td class="w-100 fill-in-area fill-in-area-two-line"> </td>
                        </tr>
                        <tr class="hide-form">
                            <td colspan="2"></td>
                            <td>
                                <label class="custom-control custom-radio text-muted">
                                    <input name="testResults" type="radio" class="fill-in-area custom-control-input" >
                                    <span class="fill-in-element custom-control-indicator"></span>
                                    <span class="custom-control-description">{{T "successful" .}}</span>
                                </label>
                                <label class="custom-control custom-radio text-muted">
                                    <input name="testResults" type="radio" class="fill-in-area custom-control-input" >
                                    <span class="fill-in-element custom-control-indicator"></span>
                                    <span class="custom-control-description">{{T "partly failed" .}}</span>
                                </label>
                                <label class="custom-control custom-radio text-muted">
                                    <input name="testResults" type="radio" class="fill-in-area custom-control-input" >
                                    <span class="fill-in-element custom-control-indicator"></span>
                                    <span class="custom-control-description">{{T "Failed" .}}</span>
                                </label>
                                <label class="custom-control custom-radio text-muted">
                                    <input name="testResults" type="radio" class="fill-in-area custom-control-input" >
                                    <span class="fill-in-element custom-control-indicator"></span>
                                    <span class="custom-control-description">{{T "not evaluated" .}}</span>
                                </label>
                            </td>
                        </tr>
                    {{ end }}
                        <tr>
                            <td colspan="3"><strong>{{T "Summary" .}}</strong></td>
                        </tr>
                        <tr class="hide-form">
                            <td></td>
                            <td valign="top" class="w-25">{{T "Notes" .}}: </td>
                            <td class="w-100 fill-in-area fill-in-area-two-line"> </td>
                        </tr>
                        <tr class="hide-form">
                            <td colspan="2"></td>
                            <td>
                                <label class="custom-control custom-radio text-muted">
                                    <input name="testResults" type="radio" class="fill-in-area custom-control-input" >
                                    <span class="fill-in-element custom-control-indicator"></span>
                                    <span class="custom-control-description">{{T "successful" .}}</span>
                                </label>
                                <label class="custom-control custom-radio text-muted">
                                    <input name="testResults" type="radio" class="fill-in-area custom-control-input" >
                                    <span class="fill-in-element custom-control-indicator"></span>
                                    <span class="custom-control-description">{{T "partly failed" .}}</span>
                                </label>
                                <label class="custom-control custom-radio text-muted">
                                    <input name="testResults" type="radio" class="fill-in-area custom-control-input" >
                                    <span class="fill-in-element custom-control-indicator"></span>
                                    <span class="custom-control-description">{{T "Failed" .}}</span>
                                </label>
                                <label class="custom-control custom-radio text-muted">
                                    <input name="testResults" type="radio" class="fill-in-area custom-control-input" >
                                    <span class="fill-in-element custom-control-indicator"></span>
                                    <span class="custom-control-description">{{T "not evaluated" .}}</span>
                                </label>
                            </td>
                        </tr>
                </table>
                {{ else }}
                <p class="text-muted">{{T "No Test Cases" .}}</p>
                {{ end }}
            </div>
            <div class="form-group">
                <label><strong>Applicability</strong></label>
                {{ if .Versions}}
                <div class="form-group">
                    <table id="versionsVariantTable" class="non-collapsed-table w-100">
                        <tr>
                            <td class="w-25"><strong>{{T "Possible Versions" .}}</strong></td>
                            <td class="w-100"><strong>{{T "Possible Variants" .}}</strong></td>
                        </tr>
                        {{ range .Versions }}
                        <tr>
                            <td class="br-4">{{ .Name }}:</td>
                            <td>
                                {{ if .Variants }}
                                    {{ range .Variants }}
                                       {{ .Name }},
                                    {{ end }}
                                {{ else }}
                                    <span class="text-muted">{{T "No applicable variants in this version" .}}.</span>span>
                                {{ end }}
                            </td>
                        </tr>
                        {{ end }}
                    </table>
                    <table class="hide-form mt-4 w-100 non-collapsed-table">
                        <tbody>
                            <tr>
                                <td class="w-25">{{T "Executed Version" .}}:</td>
                                <td class="w-100 fill-in-area"> </td>
                            </tr>
                            <tr>
                                <td class="w-25">{{T "Executed Variant" .}}:</td>
                                <td class="w-100 fill-in-area"> </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                {{ else }}
                <div class="form-group">
                    <p class="text-muted">{{T "This test case is not applicable to any system-under-test variants" .}}</p>
                </div>
                {{ end }}
            </div>
            <div class="form-group">
                <label><strong>{{T "Estimated Test Duration" .}}</strong></label>
                <p class="text-muted">
                    {{ if and (eq $.DurationHours 0) (eq $.DurationMin 0) }}
                    {{T "No Test Duration" .}}
                    {{ else }}
                    {{ if gt $.DurationHours 0 }}
                    {{ $.DurationHours }}
                    {{ if eq $.DurationHours 1 }} Hour {{ else }} Hours {{ end }}
                    {{ end }}
                    {{ if gt $.DurationMin 0 }}
                    {{ $.DurationMin }}
                    {{ if eq $.DurationMin 1 }}
                    Minute
                    {{ else }}
                    Minutes
                    {{ end }}
                    {{ end }}
                    {{ end }}
                </p>
            </div>
            {{ end }}
            {{ with .TestCase }}
            <div class="form-group">
                <label><strong>{{T "Labels" .}}</strong></label>
                <p>
                    {{ range .Labels }}
                        <span   id="container-label-{{ .Id }}"
                                class="badge badge-primary clickIcon"
                                data-toggle="tooltip"
                                data-original-title="{{ .Description }}"
                                style="background-color: {{ .Color }};
                                        color: {{.TextColor}}">{{ .Name }}</span>
                    {{ else }}
                    <span class="text-muted">{{T "No Labels" .}}</span>
                    {{ end }}
                </p>
            </div>
            {{ end }}
        </div>
    </div>
</div>

<!-- Import Scripts here -->

<script>
    var checkbox = $("#showFormCheckbox");
    checkbox.addClass("d-inline");
    checkbox.removeClass("d-none");
    $("#showForm").change(function(){
        if(this.checked) {
            $(".hide-form").removeClass("hide-form").addClass("show-form");
        } else {
            $(".show-form").removeClass("show-form").addClass("hide-form");
        }
    });
</script>
{{ end }}