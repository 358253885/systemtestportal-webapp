{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
<link rel="stylesheet" type="text/css" href="/static/css/project/reorder-utils.css" integrity="{{sha256 "/static/css/project/reorder-utils.css"}}">

<div class="modal fade" id="helpModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{T "Test Sequence Details" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="help-table">
                    <tr>
                        <td colspan="3">
                            {{T "In this view you can change the details of a test sequence" .}}.
                        </td>
                    </tr>
                    <tr class="h5">
                        <th>{{T "Buttons" .}}</th>
                        <th>{{T "Function" .}}</th>
                        <th><span class="d-none d-sm-inline">{{T "Shortcut" .}}</span></th>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-secondary">
                                <i class="fa fa-times" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> Abort</span>
                            </button>
                        </td>
                        <td>{{T "Abort editing and get back to the test sequence details" .}}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>Esc</button></td>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-success" type="button">
                                <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Save" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Confirm changes, save them and return back to the details" .}}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>S</button></td>
                    </tr>
                    <tr>
                        <td colspan="3"><h5>{{T "Form Fields" .}}</h5></td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{T "Test Sequence Name" .}}</strong>
                        </td>
                        <td colspan="2">{{T "Enter a short but descriptive name for the test sequence" .}}.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{T "Test Sequence Description" .}}</strong>
                        </td>
                        <td colspan="2">{{T "Describe the purpose of that test sequence in some sentences" .}}.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{T "Test Sequence Conditions" .}}</strong>
                        </td>
                        <td colspan="2">{{T "State some conditions that must be fulfilled prior to test sequence execution" .}}.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{T "Test Cases" .}}</strong>
                        </td>
                        <td colspan="2">{{T "Add some test cases here" .}}.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{T "Versions & Variants" .}}</strong>
                        </td>
                        <td colspan="2">{{T "Based on your selected test cases, these are automatically calculated" .}}.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{T "Estimated Test Time" .}} (hh:mm)</strong>
                        </td>
                        <td colspan="2">{{T "Based on your selected test cases, it's automatically calculated" .}}.</td>
                    </tr>
                </table>
                <span class="mt-3 float-left">{{T "For more information visit our" .}} <a href="http://docs.systemtestportal.org" target="_blank">{{T "documentation" .}}</a>.</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-testsequence-save" tabindex="-1" role="dialog" aria-labelledby="modal-testsequence-save-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-testsequence-save-label">{{T "Save Test Sequence" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <!-- don't beak the following two lines, otherwise the placeholder will not be diplaced properly-->
                    <div class="md-area">
                    <textarea class="form-control markdown-textarea" id="inputCommitMessage" rows="4"
                      placeholder="{{T "Why did you edit the test sequence? (Optional)" .}}"></textarea>
                        <div class="md-area-bottom-toolbar">
                            <div style="float:left">{{T "Markdown supported" .}}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="checkbox mr-auto">
                    <label><input type="checkbox" id="minorUpdate">
                        <abbr title="This is only a minor edit">{{T "Minor Change" .}}</abbr>
                    </label>
                </div>
                <button id="buttonSave" type="button" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="tab-card card" id="tabTestsequences">
    <script>
        var sequencelist = {{.TestSequences}};
        var sequences = [];
        for (sequence of sequencelist) {
            if(sequence !== {{ .TestSequence.Name }}) sequences.push(sequence);
        }
    </script>
    <form id="testSequenceEdit">
        <nav class="navbar navbar-light action-bar p-3">
            <div class="input-group flex-nowrap">
                <button class="btn btn-secondary" id="buttonAbortEdit">
                    <i class="fa fa-times" aria-hidden="true"></i>
                    <span class="d-none d-sm-inline"> {{T "Abort" .}}</span>
                </button>
                <button id="buttonSaveTestSequence" type="button" class="btn btn-success ml-2"
                        data-toggle="modal" onclick="checkForChanges()" disabled>
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                    <span class="d-none d-sm-inline"> {{T "Save" .}}</span>
                </button>
            </div>
        </nav>
        <div class="row tab-side-bar-row">
            <div class="col-md-9 p-3">
                <h4 class="mb-3">{{T "Edit" .}} {{ .TestSequence.Name }}</h4>
                <div class="form-group">
                    <label for="inputTestSequenceName"><strong>{{T "Test Sequence Name" .}}</strong></label>
                    <input class="form-control" id="inputTestSequenceName" name="inputTestSequenceName"
                           placeholder="Enter test sequence name, e.g. 'Buy a short distance ticket'" value="{{ .TestSequence.Name }}" required>
                </div>
                {{ with .TestSequenceVersion }}
                <div class="form-group">
                    <label for="inputTestSequenceDescription"><strong>{{T "Test Sequence Description" .}}</strong></label>
                    <div class="md-area">
                        <textarea class="form-control markdown-textarea" id="inputTestSequenceDescription" rows="4"
                                  placeholder="{{T "Describe the test sequence, e.g. 'This test sequence tests ticket buy options'" .}}">{{ .Description }}</textarea>
                        <div class="md-area-bottom-toolbar">
                            <div style="float:left">{{T "Markdown supported" .}}</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="mt-2" for="inputTestSequencePreconditions"><strong>{{T "Test Sequence Preconditions" .}}</strong></label>
                    {{/*<button type="button" class="btn btn-sm btn-primary float-right" id="preconditionAdder">*/}}
                        {{/*Add Condition*/}}
                    {{/*</button>*/}}

                    <ul class="list-group" id="preconditionsList">
                     {{ if .Preconditions }} 
                        {{ range $index, $elem := .Preconditions }}
                        <li class="list-group-item preconditionItem" >
                            <span>{{ $elem.Content }}</span>
                            <button class="close ml-2 list-line-item btn-sm deletePrecondition pull-right" type="button">
                                    <span class="d-none d-sm-inline"> x</span>
                            </button>
                        </li>
                        {{ end }}
                    {{ end }} 
                    </ul>
                    <div class="input-group">
                        <span class="input-group-btn">
                        <button type="button" class="btn btn-primary" id="preconditionAdder">
                            <i class="fa fa-plus"></i>
                        </button>
                        </span>
                        <input class="form-control width100" id="preconditionInput" placeholder="{{T "New Precondition" .}}">
                    </div>
                </div>
                    <label class="mt-2 mb-0"><strong>Test Cases</strong></label>
                    <button type="button" class="btn btn-sm btn-primary float-right" id="buttonAddTestcase"
                            data-toggle="modal" data-target="#modal-testcase-selection">
                        {{T "Add Test Case" .}}
                    </button>
                <div id="sortable-container" class="form-group">
                    <ul id="testCasesList" class="list-group sortable pb-2 pt-2">
                        {{ if .Cases }}
                            {{ range .Cases }}
                            <li class="list-group-item testCaseInTestSequence" id="{{ .Name }}">
                                <button type="button" class="test-sequence-reorder">
                                    <i class="fa fa-bars"></i>
                                </button>
                                <span>{{ .Name }}</span>
                                <button type="button" class="remove-test-case close" aria-label="Remove">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </li>
                            {{ end }}
                        {{ else }}
                            <li class="list-group-item" id="testCaseListPlaceholder">
                                <span class="text-muted">No Test Cases</span>
                                <a type="button" class="add-test-case close ml-auto" aria-label="Add TestCase"
                                   data-toggle="modal" data-target="#modal-testcase-selection">
                                    <span aria-hidden="true">+</span>
                                </a>
                            </li>
                        {{ end }}
                    </ul>
                </div>
            </div>
            <div class="col-md-3 p-3 tab-side-bar">
                <div class="form-row">
                    <div class="form-group">
                        <label for="inputTestsequenceTime"><strong>{{T "Estimated Test Duration" .}}</strong></label>
                        <p class="text-muted" id="testsequenceTime">
                             {{ if and (eq .SequenceInfo.DurationHours 0) (eq .SequenceInfo.DurationMin 0) }}
                             No Test Duration
                             {{ else }}
                                 {{ if gt .SequenceInfo.DurationHours 0 }}
                                     {{ .SequenceInfo.DurationHours }}
                                     {{ if eq .SequenceInfo.DurationHours 1 }} Hour {{ else }} Hours {{ end }}
                                 {{ end }}
                                 {{ if gt .SequenceInfo.DurationMin 0 }}
                                    {{ .SequenceInfo.DurationMin }}
                                    {{ if eq .SequenceInfo.DurationMin 1 }}
                                        Minute
                                    {{ else }}
                                        Minutes
                                    {{ end }}
                                 {{ end }}
                             {{ end }}
                             {{ end }}
                        </p>
                    </div>
                </div>
            </div>
            {{template "modal-testcase-selection" . }}
            {{template "modal-manage-versions" . }}
        </div>
    </form>
</div>

<!-- Import Scripts here -->
<script src="/static/js/project/testsequences.js" integrity="{{sha256 "/static/js/project/testsequences.js"}}"></script>
<script src="/static/js/project/tests.js" integrity="{{sha256 "/static/js/project/tests.js"}}"></script>
<script src="/static/assets/js/vendor/jquery.validate.min.js" integrity="{{sha256 "/static/assets/js/vendor/jquery.validate.min.js"}}"></script>

<script type="text/javascript">

    // Save the original test sequence. It is needed to check for changes.
    testsequence = {{ .TestSequence }};
</script>
<script>
    $('document').ready(function() {
            if($("#testSequenceEdit").valid()) {
                $('#buttonSaveTestSequence').removeAttr("disabled");
            }
        }
    );
    assignEventsToNameTextField();
    assignButtonsTestSequence();
    assignPreconditionInputListener();

    reloadDragAndDropFeature()
</script>
{{end}}