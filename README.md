[![Build-Badge][build]][commits-master]
[![Coverage-Badge][coverage]][coverage-report]
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/stp-team/systemtestportal-webapp)](https://goreportcard.com/report/gitlab.com/stp-team/systemtestportal-webapp)
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

[coverage]: https://gitlab.com/358253885/systemtestportal-webapp/badges/master/coverage.svg?job=test-project "Master test coverage"
[build]: https://gitlab.com/stp-team/systemtestportal-webapp/badges/master/build.svg "Master pipeline status"
[coverage-report]: https://gitlab.com/358253885/systemtestportal-webapp/builds/artifacts/master/browse?job=test-project
[commits-master]: https://gitlab.com/stp-team/systemtestportal-webapp/commits/master



# SystemTestPortal - http://www.systemtestportal.org

SystemTestPortal is a web application that allows to create, run and analyze
manual system tests. It aims to be useful for developers, testers and end-users.

## Features

- Dashboard that provides an overview of the latest execution results for test cases and test sequences
- Step-by-step execution of tests
- Organizing test cases with the help of test sequences
- Inspect protocols of test executions
- Export execution protocols to PDF
- Print tests to execute them offline
- Social features to manage test creation and test execution within a project

Discover more on our [Homepage](http://www.systemtestportal.org/discover/).

## Quick start

### Binaries

Once you have [downloaded](ftp://ftp.informatik.uni-stuttgart.de/pub/se/systemtestportal/) the 
binary or compiled it yourself. The server can be started using:

```
stp --host=<the hostname (e.g. localhost or 0.0.0.0)> --port=<the port to listen to>
```

If no port is provided the server will listen to all interfaces on port 8080 by default.

### Docker image

The easiest way to get an STP up and running is by using our Docker Image.

We offer a docker image via [dockerhub](https://hub.docker.com/r/systemtestportal/systemtestportal/)
or our [registry on gitlab](https://gitlab.com/stp-team/systemtestportal-webapp/container_registry)

You can simple pull the latest image using:

```bash
docker pull systemtestportal/systemtestportal
```

To pull a specific tag you can run for example:

```bash
docker pull systemtestportal/systemtestportal:v1-4-0
```

Just run the following command to start the container:

```bash
docker run -p 8080:8080 systemtestportal/systemtestportal:latest
```

You can pull the most current image of our master branch from our 
[registry on gitlab](https://gitlab.com/stp-team/systemtestportal-webapp/container_registry)

```bash
docker pull registry.gitlab.com/stp-team/systemtestportal-webapp/signed:master
```

# Documentation

[Click here](http://docs.systemtestportal.org/) to checkout our full user documentation.

# Configuration

STP can be configured using a ini configuration file. The default path for the configuration file on
linux is `/etc/stp/config.ini`. On windows STP looks for the config.ini inside the working directory
(the directory STP is started in).
You can define a different path to the configuration file via the commandline flag
`--config-path=<your custom path to the configuration>`.

A configuration file with all defaults can be found [here](./config.ini).

# Flags

The available flags you can pass to STP are as follows:

 - `--basepath=<the base path>`: This is directory STP searches for its resource folders
 (templates, static, migrations, data).
 - `--port=<port number>`: The port that STP should listen on.
 - `--host=<host>`: The host that STP should interface with.
 - `--debug`: If present the log output will be a bit more verbose.
 - `--config-path=<path/to/the/configuration>`: Sets the path where STP searches for its
 configuration file.
 - `--data-dir=<path/where/stp/stores/date>`: The directory that STP should stores its data
 in. E.g. when build with sqlite STP will store the database file there.

## Installation guide

The installation guide can be found on [our website](http://www.systemtestportal.org/get/).

## Do you want to contribute?

Check out our [workflow](https://gitlab.com/stp-team/systemtestportal-webapp/blob/master/CONTRIBUTING.md).

## About the developers

The SystemTestPortal is developed by students of the
[Institute of Software Technology at the Universität Stuttgart](http://www.iste.uni-stuttgart.de/se) 
in a student project.

## License

[GNU GPL-3.0](https://opensource.org/licenses/GPL-3.0)