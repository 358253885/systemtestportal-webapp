# Use Names as unique IDs to 

* Status: [accepted | superseeded by [ADR-0005](0005-example.md) | deprecated | …] <!-- optional -->
* Deciders: STP 1.0 Team <!-- optional -->
* Date: documented 29.06.2018 <!-- optional -->

Technical Story: For Reference Issue #556

## Decision Outcome

The STP 1.0 team decided to use names as unique ids in order to have unique URLs that consist of the test case names and are readable.

"The reason why we used the names as unique ids was that we wanted to have readable URLs (/demo/SystemTestPortal/testcases/Add%20test%20steps)."
