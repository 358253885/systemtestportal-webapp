/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package middleware

import (
	"net/http"

	"context"

	"github.com/dimfeld/httptreemux"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
)

const (
	issueTracker = "<a href='" +
		"https://gitlab.com/stp-team/systemtestportal-webapp/issues'>issue tracker" +
		"</a>"
	noRoutingParamTitle = "We couldn't handle your request."
	noRoutingParam      = "It seems like you found a bug" +
		" in our code. Please inform contact us via our " + issueTracker + "."
	internalErrorTitle = "We are sorry :/"
	internalErrorMsg   = "It seems we were unable to handle " +
		"your request please try reloading. If this error persists please " +
		"let us know about this error via our " + issueTracker + "."
)

func noRoutingParamError(r *http.Request, name string) errors.HandlerError {
	return errors.ConstructStd(http.StatusInternalServerError, noRoutingParamTitle, noRoutingParam, r).
		WithLogf("Error while routing. Container "+
			"parameter with name '%s' wasn't received.", name).
		WithStackTrace(2).
		WithRequestDump(r).
		Finish()
}

func internalError(logMsg string, cause error, r *http.Request) errors.HandlerError {
	return errors.ConstructStd(http.StatusInternalServerError, internalErrorTitle, internalErrorMsg, r).
		WithLog(logMsg).
		WithStackTrace(2).
		WithCause(cause).Finish()
}

// AddToContext adds a value to given request context.
func AddToContext(r *http.Request, key, value interface{}) {
	*r = *r.WithContext(context.WithValue(r.Context(), key, value))
}

func getParams(r *http.Request) map[string]string {
	return httptreemux.ContextParams(r.Context())
}

func getParam(r *http.Request, name string) (string, error) {
	params := getParams(r)
	value, ok := params[name]
	if !ok {
		return "", noRoutingParamError(r, name)
	}
	return value, nil
}
