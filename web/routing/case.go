/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package routing

import (
	"github.com/urfave/negroni"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/assignment"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/creation"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/deletion"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/display"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/duplication"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/execution"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/json"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/list"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/printing"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/update"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
	"gitlab.com/stp-team/systemtestportal-webapp/web/sessions"
)

func registerCasesHandler(pg *contextGroup, n *negroni.Negroni, testCaseStore store.CasesSQL) {
	activityStore := store.GetActivityStore()
	labelStore := store.GetLabelStore()

	tcsg := wrapContextGroup(pg.NewContextGroup(Cases))

	tcsg.HandlerGet(Show,
		n.With(negroni.WrapFunc(list.CasesGet(testCaseStore))))
	tcsg.HandlerGet(New,
		n.With(negroni.WrapFunc(display.CreateCaseGet(testCaseStore))))
	tcsg.HandlerGet(Save,
		n.With(negroni.Wrap(defaultRedirect("../"))))
	tcsg.HandlerGet(JSON,
		n.With(negroni.WrapFunc(json.CasesGet(testCaseStore))))
	tcsg.HandlerGet(Print,
		n.With(negroni.WrapFunc(printing.CasesListGet(testCaseStore))))

	tcsg.HandlerPost(Save,
		n.With(negroni.WrapFunc(creation.CasePost(testCaseStore, testCaseStore, labelStore, activityStore))))

	registerCaseHandler(tcsg, n, testCaseStore)
}

func registerCaseHandler(tcsg *contextGroup, n *negroni.Negroni, testCaseStore store.CasesSQL) {
	userStore := store.GetUserStore()

	sessionStore := sessions.GetSessionStore()
	protocolStore := store.GetProtocolStore()

	taskStore := store.GetTaskStore()
	commentStore := store.GetCommentStore()
	activityStore := store.GetActivityStore()

	tcg := wrapContextGroup(tcsg.NewContextGroup(VarCase))

	tcg.HandlerGet(Show, n.With(middleware.Testcase(testCaseStore), negroni.WrapFunc(display.ShowCaseGet(commentStore, taskStore, testCaseStore))))
	tcg.HandlerPut(Show, n.With(middleware.Testcase(testCaseStore), negroni.WrapFunc(creation.CaseCommentPut(commentStore))))
	tcg.HandlerGet(Edit,
		n.With(middleware.Testcase(testCaseStore), negroni.WrapFunc(display.EditCaseGet(testCaseStore))))
	tcg.HandlerGet(History,
		n.With(middleware.Testcase(testCaseStore), negroni.WrapFunc(display.HistoryCaseGet)))
	tcg.HandlerGet(JSON,
		n.With(middleware.Testcase(testCaseStore), negroni.WrapFunc(json.CaseGet)))
	tcg.HandlerGet(Print,
		n.With(middleware.Testcase(testCaseStore), negroni.WrapFunc(printing.CaseGet)))

	tcg.HandlerPost(Duplicate,
		n.With(middleware.Testcase(testCaseStore), negroni.WrapFunc(duplication.CasePost(testCaseStore, testCaseStore))))

	tcg.HandlerPut(Update,
		n.With(middleware.Testcase(testCaseStore), negroni.WrapFunc(update.CasePut(testCaseStore, testCaseStore, commentStore, taskStore, testCaseStore))))
	tcg.HandlerPut(Tester,
		n.With(middleware.Testcase(testCaseStore), negroni.WrapFunc(assignment.CasePut(userStore, taskStore, taskStore, taskStore, taskStore, activityStore))))

	tcg.HandlerDelete("",
		n.With(middleware.Testcase(testCaseStore), negroni.WrapFunc(deletion.CaseDelete(testCaseStore))))

	registerCaseExecuteHandler(tcg, n, testCaseStore, sessionStore, protocolStore, testCaseStore, taskStore, taskStore, activityStore)
	registerCaseLabelsHandler(tcg, n, testCaseStore)
}

func registerCaseExecuteHandler(tcg *contextGroup, n *negroni.Negroni,
	tcs middleware.TestCaseStore, session *sessions.Store, protocol store.ProtocolsSQL,
	caseLister handler.TestCaseLister, taskGetter handler.TaskListGetter, taskAdder handler.TaskListAdder, activityStore handler.Activities) {

	projectStore := store.GetProjectStore()

	tcg.HandlerGet(Execute,
		n.With(middleware.Testcase(tcs), negroni.WrapFunc(execution.CaseStartPageGet())))
	tcg.HandlerPost(Execute,
		n.With(middleware.Testcase(tcs), negroni.WrapFunc(execution.CaseExecutionPost(protocol, protocol,
			nil, nil, nil, tcs, caseLister, taskAdder, taskGetter, activityStore,
			projectStore, projectStore))))
	tcg.HandlerPost(Execute+Upload,
		n.With(middleware.Testcase(tcs), negroni.WrapFunc(execution.Upload())))
}

func registerCaseLabelsHandler(tcg *contextGroup, n *negroni.Negroni, tcs store.CasesSQL) {
	labelStore := store.GetLabelStore()

	labelContextGroup := wrapContextGroup(tcg.NewContextGroup(Labels))

	labelContextGroup.HandlerGet(Show,
		n.With(middleware.Testcase(tcs), negroni.WrapFunc(json.TestLabelsGet(labelStore))))
	labelContextGroup.HandlerPost(New,
		n.With(middleware.Testcase(tcs), negroni.WrapFunc(creation.TestLabelPost(labelStore))))
	labelContextGroup.HandlerPut(Delete,
		n.With(middleware.Testcase(tcs), negroni.WrapFunc(deletion.DeleteTestLabelPut(labelStore))))
}
