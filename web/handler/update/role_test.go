/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"net/http"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestProjectRolesPut(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
		},
	)

	// Use {} instead of [] because roles is a map and not an array
	body := "{}"

	handler.Suite(t,
		handler.CreateTest("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.ProjectAdderMock{}
				return ProjectRolesPut(a), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(a, 0),
				)
			},
			handler.NewRequest(invalidCtx, http.MethodPut, handler.NoParams, body),
			handler.NewFragmentRequest(invalidCtx, http.MethodPut, handler.NoParams, body),
		),
		handler.CreateTest("Invalid body",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.ProjectAdderMock{}
				return ProjectRolesPut(a), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
					handler.HasCalls(a, 0),
				)
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, ""),
			handler.NewFragmentRequest(ctx, http.MethodPut, handler.NoParams, ""),
		),
		handler.CreateTest("Adder returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.ProjectAdderMock{Err: handler.ErrTest}
				return ProjectRolesPut(a), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(a, 1),
				)
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, body),
			handler.NewFragmentRequest(ctx, http.MethodPut, handler.NoParams, body),
		),
		handler.CreateTest("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.ProjectAdderMock{}
				return ProjectRolesPut(a), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(a, 1),
				)
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, body),
			handler.NewFragmentRequest(ctx, http.MethodPut, handler.NoParams, body),
		),
	)
}
