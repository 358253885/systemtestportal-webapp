/*
 * This file is part of SystemTestPortal.
 * Copyright (C) 2017-2018  Institute of Software Technology, University of Stuttgart
 *
 * SystemTestPortal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SystemTestPortal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
 */

package update

import (
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestEditProfilePut(t *testing.T) {
	store.InitializeTestDatabase()
	ul := handler.UserListerMock{}
	ul.Initialize(handler.DummyUser, handler.DummyUserUnauthorized)
	updater := handler.UserUpdaterMock{}

	params := url.Values{}
	params.Add("user", handler.DummyUser.Name)
	params.Add("email", "test@test.com")
	params.Add("bio", "Lorem ipsum")
	params.Add("isShownPublic", "true")
	params.Add("isEmailPublic", "true")

	invalidParams := url.Values{}
	invalidParams.Add("user", "notExisting")
	invalidParams.Add("email", "test@test.com")
	invalidParams.Add("bio", "Lorem ipsum")
	invalidParams.Add("isShownPublic", "true")
	invalidParams.Add("isEmailPublic", "true")

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: handler.DummyUser,
		},
	)
	unauthorizedCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: handler.DummyUserUnauthorized,
		},
	)

	tested := EditProfilePut(&ul, &updater)
	handler.Suite(t,
		handler.CreateTest("Normal case",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(ctx, http.MethodPut, params),
		),
		handler.CreateTest("Unauthorized User",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusForbidden),
			),
			handler.SimpleRequest(unauthorizedCtx, http.MethodPut, params),
		),
		handler.CreateTest("No User signed in",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusForbidden),
			),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, params),
		),
		handler.CreateTest("Invalid Profile",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusNotFound),
			),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, invalidParams),
		),
	)
}

func TestEditPasswordPut(t *testing.T) {
	store.InitializeTestDatabase()
	//us := store.GetUserStore()
	ul := handler.UserListerMock{}
	ul.Initialize(handler.DummyUser, handler.DummyUserUnauthorized)
	ua := handler.UserAdderMock{}
	uv := handler.UserValidatorMock{}

	params := url.Values{}
	params.Add("user", handler.DummyUser.Name)
	params.Add("userPassword", "secret")
	params.Add("newPassword", "")

	invalidParams := url.Values{}
	invalidParams.Add("user", "notExisting")
	invalidParams.Add("userPassword", "secret")
	invalidParams.Add("newPassword", "")

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: handler.DummyUser,
		},
	)
	unauthorizedCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: handler.DummyUserUnauthorized,
		},
	)

	tested := EditPasswordPut(&ul, &ua, &uv)
	handler.Suite(t,
		handler.CreateTest("Normal case",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(ctx, http.MethodPut, params),
		),
		handler.CreateTest("Unauthorized User",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusForbidden),
			),
			handler.SimpleRequest(unauthorizedCtx, http.MethodPut, params),
		),
		handler.CreateTest("No User signed in",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusForbidden),
			),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, params),
		),
		handler.CreateTest("Invalid Profile",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusNotFound),
			),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, invalidParams),
		),
	)
}

func TestDeactivateUserPut(t *testing.T) {
	uv := handler.UserValidatorMock{}
	uu := handler.UserUpdaterMock{}

	validParams := url.Values{}
	validParams.Add("user", handler.DummyUser.Name)
	validParams.Add("password", "secret")

	invalidUser := url.Values{}
	invalidUser.Add("user", "asdf")
	invalidUser.Add("password", "asdfasdf")

	invalidPassword := url.Values{}
	invalidPassword.Add("user", handler.DummyUser.Name)
	invalidPassword.Add("password", "asdf")

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: handler.DummyUser,
		},
	)
	unauthorizedCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: handler.DummyUserUnauthorized,
		},
	)
	tested := DeactivateUserPut(&uu, &uv)
	handler.Suite(t,
		handler.CreateTest("Normal case",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(ctx, http.MethodPut, validParams),
		),
		handler.CreateTest("Unauthorized User",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusForbidden),
			),
			handler.SimpleRequest(unauthorizedCtx, http.MethodPut, validParams),
		),
		handler.CreateTest("No User signed in",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusForbidden),
			),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, validParams),
		),
		handler.CreateTest("Invalid Password",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusUnauthorized),
			),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, invalidPassword),
		),
		handler.CreateTest("Invalid User",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusUnauthorized),
			),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, invalidUser),
		),
	)
}
