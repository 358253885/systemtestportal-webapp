/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"net/http"
			"gitlab.com/stp-team/systemtestportal-webapp/domain/group"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity"
)

// InvalidRequest returns an error describing that
// the request contained no or invalid information.
func InvalidRequest(r *http.Request) errors.HandlerError {
	return errors.DefaultError(http.StatusInternalServerError).
		WithLog("error retrieving the entities of the context").
		WithRequestDump(r).
		WithStackTrace(1).
		Finish()
}

// ContextEntities contains every entity that a handler
// could need for handling requests. These are
// entities like the project, user, case, sequence etc.
type ContextEntities struct {
	User             *user.User
	ContainerID      id.ActorID
	Project          *project.Project
	Case             *test.Case
	Sequence         *test.Sequence
	CaseProtocol     *test.CaseExecutionProtocol
	SequenceProtocol *test.SequenceExecutionProtocol
	Err              error
}

// GetContextEntities returns all entities that are
// in the request. Entities are a project, user
// case, sequence or container. If an entity could
// not be retrieved, the entity is nil.
//
// Any error that occurs while retrieving the entities
// is logged. The Err in the ContextEntities is an
// InvalidRequest error.
func GetContextEntities(r *http.Request) *ContextEntities {

	// Errors will be removed after context entity refactoring (id #374)
	c, _ := getTestCase(r)
	s, _ := getTestSequence(r)
	caseProtocol, _ := getCaseProtocol(r)
	sequenceProtocol, _ := getSequenceProtocol(r)
	p, _ := getProject(r)
	u, _ := getUser(r)
	cid, _ := getContainerID(r)

	return &ContextEntities{
		User:             u,
		ContainerID:      cid,
		Project:          p,
		Case:             c,
		Sequence:         s,
		CaseProtocol:     caseProtocol,
		SequenceProtocol: sequenceProtocol,
		Err:              InvalidRequest(r),
	}
}

// getValue gets a value with given key from given request.
func getValue(r *http.Request, key interface{}) interface{} {
	return r.Context().Value(key)
}

// getProject gets the project from given request if it
// was stored there by middleware. If the project is nil
// or not contained in the request an error will be returned
// and the returned project will be nil.
func getProject(r *http.Request) (*project.Project, error) {
	v := getValue(r, middleware.ProjectKey)
	p, ok := v.(*project.Project)
	if ok && p != nil {
		return p, nil
	}
	return nil, InvalidRequest(r)
}

// getTestCase gets the testcase from given request if it
// was stored there by middleware. If the testcase is nil
// or not contained in the request an error will be returned
// and the returned testcase will be nil.
func getTestCase(r *http.Request) (*test.Case, error) {
	v := getValue(r, middleware.TestCaseKey)
	tc, ok := v.(*test.Case)
	if ok && tc != nil {
		return tc, nil
	}
	return nil, InvalidRequest(r)
}

// getCaseProtocol gets the case protocol from given request if it
// was stored there by middleware. If the protocol is nil
// or not contained in the request an error will be returned
// and the returned protocol will be nil.
func getCaseProtocol(r *http.Request) (*test.CaseExecutionProtocol, error) {
	v := getValue(r, middleware.ProtocolKey)
	protocol, ok := v.(*test.CaseExecutionProtocol)
	if ok && protocol != nil {
		return protocol, nil
	}
	return nil, InvalidRequest(r)
}

// getSequenceProtocol gets the sequence protocol from given request if it
// was stored there by middleware. If the protocol is nil
// or not contained in the request an error will be returned
// and the returned protocol will be nil.
func getSequenceProtocol(r *http.Request) (*test.SequenceExecutionProtocol, error) {
	v := getValue(r, middleware.ProtocolKey)
	protocol, ok := v.(*test.SequenceExecutionProtocol)
	if ok && protocol != nil {
		return protocol, nil
	}
	return nil, InvalidRequest(r)
}

// getTestSequence gets the testsequence from given request if it
// was stored there by middleware. If the testsequence is nil
// or not contained in the request an error will be returned
// and the returned testsequence will be nil.
func getTestSequence(r *http.Request) (*test.Sequence, error) {
	v := getValue(r, middleware.TestSequenceKey)
	ts, ok := v.(*test.Sequence)
	if ok && ts != nil {
		return ts, nil
	}
	return nil, InvalidRequest(r)
}

// getUser gets the currently signed in user from given
// request if it was stored there by middleware. If the user
// is not contained in the request an error will be
// returned and the returned user will be nil.
// If no user is signed in the user will be nil as well as the
// returned error.
func getUser(r *http.Request) (*user.User, error) {
	v := getValue(r, middleware.UserKey)
	u, ok := v.(*user.User)
	if ok {
		return u, nil
	}
	return nil, InvalidRequest(r)
}

// getContainer gets the container (user or group) from given
// request if it was stored there by middleware. If the container
// is nil or not contained in the request an error will be returned
// and the returned container will be nil.
func getContainer(r *http.Request) (*group.Group, *user.User, error) {
	v := getValue(r, middleware.ContainerKey)
	switch container := v.(type) {
	case *group.Group:
		return container, nil, nil
	case *user.User:
		return nil, container, nil
	default:
		return nil, nil, InvalidRequest(r)
	}
}

// getContainerID gets the id of the container (user or group) from given
// request if it was stored there by middleware. If the container
// is nil or not contained in the request an error will be returned
// and the returned container id will be an empty id.
func getContainerID(r *http.Request) (id.ActorID, error) {
	g, u, err := getContainer(r)
	if err != nil {
		return "", err
	}
	if g != nil {
		return g.ID(), nil
	}
	if u != nil {
		return u.ID(), nil
	}
	return "", err
}

// Do to circular dependencies, we have to pass the contextEntities in the struct format "ActivityEntities"
func (contextEntities ContextEntities) GetActivityEntities() *activity.ActivityEntities {
	return &activity.ActivityEntities{
		User:             contextEntities.User,
		Project:          contextEntities.Project,
		Case:             contextEntities.Case,
		Sequence:         contextEntities.Sequence,
		CaseProtocol:     contextEntities.CaseProtocol,
		SequenceProtocol: contextEntities.SequenceProtocol,
	}
}
