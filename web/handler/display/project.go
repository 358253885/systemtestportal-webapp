/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// CreateProjectGet is a handler for showing the screen for creating a new project
func CreateProjectGet(lister handler.ProjectLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		contextEntities := handler.GetContextEntities(r)
		if contextEntities.User == nil {
			errors.Handle(contextEntities.Err, w, r)
			return
		}

		var projects = make([]string, 0)

		projectlist, err := lister.ListAll()
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		for _, project := range projectlist {
			projects = append(projects, project.Name)
		}

		tmpl := getNewProjectTree(r)
		handler.PrintTmpl(context.New().WithUserInformation(r).With(context.Projects, projects), tmpl, w, r)
	}
}

// getNewProjectTree returns the new project template with all parent templates
func getNewProjectTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// New project tree
		Append(templates.NewProject).
		Append(templates.GenericError).
		Get().Lookup(templates.HeaderDef)

}
