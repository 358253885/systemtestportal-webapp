/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Package errors contains helpful types and structures simplifying
error handling.

Handler Errors

Handler errors consist of a status code a log message and a body.
The status code and body can be send to the client and the log message can
obviously be logged.

Errors for handlers can easily be constructed using chaining.
You can start the construction of an error with the:
		Construct(status int)
or
		ConstructWithStackTrace(status int, logMsg string)
method.
The latter is just a shortcut for:
		Construct(status).WithLog(logMsg).WithStackTrace(1)
This constructs an error with given status code and a log
message that contains the logMsg and a little stack trace showing
where the error was constructed which allows to find the root of an error
more easily.

The parameter for the WithStackTrace method determines how far up the stack
the trace should be taken from. A 1 means the trace should be taken from the
caller of the WithStackTrace method.
If you create your own construction function
for commonly constructed errors you can set this to 2 to take the trace of the
code calling your construction function instead of taking the trace from your
construction function itself.

Similarly to the  WithStackTrace(skip int) there is the WithCause(cause error)
method which appends the message of given cause to the log of the error.

Both of these method require that the WithLog(logMsg string) has been called before
them or else they will panic.

To determine the content of the errors body the following methods can be used:
		WithBody(body string)
		WriteToBody(write func(io.Writer))

The first one simply writes the given string to the body.
The second allows to directly write to the body without interrupting the chaining of calls.
It accepts a function taking a writer to which in can write the content of the body.

To finish the construction and retrieve the created error simply use the
Finish() method which will return the constructed error.

The HandlerErrorConstructor is also a io.Writer which means you can directly write to
the errors body by passing it as writer.
*/
package errors

import (
	"log"
	"net/http"

	"bytes"
	"fmt"
	"runtime"

	"io"

	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

// HandlerErrorConstructor is used to construct a HandlerError.
// It allows chaining methods that consequently define the
// errors attributes.
type HandlerErrorConstructor interface {
	// WithBody adds the given string to the body of
	// the error.
	WithBody(body string) HandlerErrorConstructor
	// WriteToBody allows to directly writing to the body of the error without
	// interrupting chaining.
	WriteToBody(write func(io.Writer)) HandlerErrorConstructor
	// WithLog adds the given message to the log entry
	// of the error.
	WithLog(logMsg string) HandlerErrorConstructor
	// WithLogf adds the given message to the log entry
	// of the error. It formats given message using
	// fmt.Sprintf.
	WithLogf(logMsg string, values ...interface{}) HandlerErrorConstructor
	// WithRequestDump adds a dump of given request to the errors log message.
	WithRequestDump(r *http.Request) HandlerErrorConstructor
	// WithStackTrace adds the line and file from the call stack
	// to the errors log message.
	//
	// The argument skip is the number of stack frames
	// to ascend, with 0 identifying the caller of this method.
	//
	// Please call WithLog before calling this method or else it
	// will panic.
	//
	// The new message looks like this:
	//
	// <current message>
	//     Created at: line <caller line> in file '<caller file>'
	WithStackTrace(skip int) HandlerErrorConstructor
	// WithCause adds the error message of given cause
	// to this errors log.
	//
	// Please call WithLog before calling this method or else it
	// will panic.
	//
	// The new message looks like this:
	//
	// <current message>
	//     Caused by: <cause.Error()>
	WithCause(cause error) HandlerErrorConstructor
	// Copy copies this constructor. Calls to the copy have no influence
	// on the constructor it was copied from. This can be used to create
	// template for error which can be copied an further modified without changing
	// the original.
	Copy() HandlerErrorConstructor
	// Finish finishes the construction and returns the constructed error.
	Finish() HandlerError
	// Respond directly finishes the construction and writes the error to given
	// response writer. The log message is also triggered.
	Respond(w http.ResponseWriter)
	// Enables direct writes to the errors body.
	io.Writer
}

// HandlerError is an error that occurs in handlers
// the defined function will be used to write the
// error to a ResponseWriter and to the log.
type HandlerError interface {
	// Error returns the error message that
	// should be logged once the error occurs.
	Error() string
	// StatusCode returns the http status code that should
	// be sent to the client.
	StatusCode() int
	// Body returns the body this error contains and
	// should be written to the client response.
	Body() string
	// Shows the log part of this error.
	fmt.Stringer
}

// handlerError is a simple implementation of a
// HandlerError.
//
// It uses a buffer so writing and reading the body can
// be easily achieved.
// The string that can be logged also uses a buffer so
// it can be written dynamically.
type handlerError struct {
	*bytes.Buffer
	statusCode int
	log        *bytes.Buffer
}

// StatusCode returns the http status code that should
// be sent to the client.
func (hErr handlerError) StatusCode() int {
	return hErr.statusCode
}

// Error returns the error message that
// should be logged once the error occurs.
func (hErr handlerError) Error() string {
	return hErr.log.String()
}

// Body returns the body this error contains and
// should be written to the client response.
func (hErr handlerError) Body() string {
	return hErr.Buffer.String()
}

// WriteToBody allows to directly writing to the body of the error without
// interrupting chaining.
func (hErr handlerError) WriteToBody(write func(io.Writer)) HandlerErrorConstructor {
	write(hErr)
	return hErr
}

// WithBody adds the given string to the body of
// the error.
func (hErr handlerError) WithBody(body string) HandlerErrorConstructor {
	_, err := hErr.WriteString(body)
	if err != nil {
		log.Panicf("Couldn't write body to error. Body was:\n%s", body)
	}
	return hErr
}

// WithLog adds the given message to the log entry
// of the error.
func (hErr handlerError) WithLog(logMsg string) HandlerErrorConstructor {
	if hErr.log.Len() > 0 {
		logMsg = fmt.Sprintf("\n%s", logMsg)
	}
	_, err := hErr.log.WriteString(logMsg)
	if err != nil {
		log.Panicf("Couldn't write log to error. Log was:\n%s", logMsg)
	}
	return hErr
}

// WithLogf adds the given message to the log entry
// of the error. It formats given message using
// fmt.Sprintf.
func (hErr handlerError) WithLogf(logMsg string, values ...interface{}) HandlerErrorConstructor {
	return hErr.WithLog(fmt.Sprintf(logMsg, values...))
}

// WithRequestDump adds a dump of given request to the errors log message.
func (hErr handlerError) WithRequestDump(r *http.Request) HandlerErrorConstructor {
	return hErr.WithLogf("Request was:\n%s", httputil.DumpRequest(r))
}

// WithStackTrace adds the line and file from the call stack
// to the errors log message.
//
// The argument skip is the number of stack frames
// to ascend, with 0 identifying the caller of this method.
//
// Please call WithLog before calling this method or else it
// will panic.
//
// The new message looks like this:
//
// <current message>
//     Created at: line <caller line> in file '<caller file>'
func (hErr handlerError) WithStackTrace(skip int) HandlerErrorConstructor {
	if hErr.log.Len() <= 0 {
		panic("WithStackTrace can't be called before calling WithLog!")
	}
	_, file, line, ok := runtime.Caller(skip + 1)
	if ok {
		hErr.WithLog(fmt.Sprintf("    Created at: %s:%d", file, line))
	}
	return hErr
}

// WithCause adds the error message of given cause
// to this errors log.
//
// Please call WithLog before calling this method or else it
// will panic.
//
// The new message looks like this:
//
// <current message>
//     Caused by: <cause.Error()>
func (hErr handlerError) WithCause(cause error) HandlerErrorConstructor {
	if hErr.log.Len() <= 0 {
		panic("WithCause can't be called before calling WithLog!")
	}
	if cause != nil {
		hErr.WithLog(fmt.Sprintf("    Caused by: %s", cause))
	}
	return hErr
}

// Copy copies this constructor. Calls to the copy have no influence
// on the constructor it was copied from. This can be used to create
// template for error which can be copied an further modified without changing
// the original.
func (hErr handlerError) Copy() HandlerErrorConstructor {
	return handlerError{
		bytes.NewBufferString(hErr.Body()),
		hErr.StatusCode(),
		bytes.NewBufferString(hErr.Error()),
	}
}

// Respond directly finishes the construction and writes the error to given
// response writer. The log message is also triggered.
func (hErr handlerError) Respond(w http.ResponseWriter) {
	Respond(hErr.Finish(), w)
}

// Finish finishes the construction and returns the constructed error.
func (hErr handlerError) Finish() HandlerError {
	return hErr
}

// String returns a string representation of the error.
// Which is the full log message this error will print.
func (hErr handlerError) String() string {
	return fmt.Sprintf("Handler error. Status: %s\n%s",
		http.StatusText(hErr.statusCode), hErr.Error())
}

// Construct lets you construct a new handler error using the
// constructor.
func Construct(status int) HandlerErrorConstructor {
	return handlerError{
		bytes.NewBufferString(""),
		status,
		bytes.NewBufferString(""),
	}
}

// ConstructWithStackTrace this method is a convenient shortcut
// for:
// Construct(status).WithLog(logMsg).WithStackTrace(0)
func ConstructWithStackTrace(status int, logMsg string) HandlerErrorConstructor {
	return Construct(status).WithLog(logMsg).WithStackTrace(2)
}

// Respond writes the given error to the given response writer and logs
// the incidence.
//
// The errors body will be written to the response writer.
// The errors status code will be written to the response writer.
//
// If the log message contained in the error isn't empty
// the following message will be written to the log:
//
// Handler error. Status: <Text representation of status code>
// <error.Error()>
func Respond(error HandlerError, w http.ResponseWriter) {
	Log(error)
	w.WriteHeader(error.StatusCode())
	_, err := fmt.Fprint(w, error.Body())
	if err != nil {
		log.Panicf("Couldn't write error body to response. Body was:\n%s", error.Body())
	}
}

// Log logs the given error.
//
// If the log message contained in the error isn't empty
// the following message will be written to the log:
//
// Handler error. Status: <Text representation of status code>
// <error.Error()>
func Log(err HandlerError) {
	if len(err.Error()) > 0 {
		log.Print(err.String())
	}
}
