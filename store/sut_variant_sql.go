// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package store

import (
	"fmt"

	"github.com/go-xorm/xorm"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

const (
	versionField = "version_id"
)

type sutVariantRow struct {
	ID        int64
	VersionID int64
	Name      string
}

func sutVariantFromRow(vr sutVariantRow) project.Variant {
	return project.Variant{
		Name: vr.Name,
	}
}

func rowFromSUTVariant(v project.Variant) sutVariantRow {
	return sutVariantRow{
		Name: v.Name,
	}
}

func saveSUTVariants(s xorm.Interface, vrID int64, nvs []project.Variant) error {
	ovrs, err := listSUTVariantRows(s, vrID)
	if err != nil {
		return err
	}

	var nvrs []sutVariantRow
	for _, nv := range nvs {
		nvr := rowFromSUTVariant(nv)
		nvr.VersionID = vrID
		nvrs = append(nvrs, nvr)
	}

	dvrs := variantRowSetMinus(ovrs, nvrs)
	uvrs := variantRowCutset(ovrs, nvrs)
	ivrs := variantRowSetMinus(nvrs, ovrs)

	err = deleteSUTVariantRows(s, dvrs...)
	if err != nil {
		return err
	}

	err = updateSUTVariantRows(s, uvrs...)
	if err != nil {
		return err
	}

	err = insertSUTVariantRows(s, ivrs...)
	return err
}

func insertSUTVariantRows(s xorm.Interface, vrs ...sutVariantRow) error {
	_, err := s.Table(sutVariantTable).Insert(&vrs)
	return err
}

func updateSUTVariantRows(s xorm.Interface, vrs ...sutVariantRow) error {
	for _, vr := range vrs {
		err := updateSUTVariantRow(s, &vr)
		if err != nil {
			return err
		}
	}

	return nil
}

func updateSUTVariantRow(s xorm.Interface, vr *sutVariantRow) error {
	aff, err := s.Table(sutVariantTable).ID(vr.ID).Update(vr)
	if err != nil {
		return err
	}

	if aff != 1 {
		return fmt.Errorf(errorNoAffectedRows, aff)
	}

	return nil
}

func deleteSUTVariantRows(s xorm.Interface, vrs ...sutVariantRow) error {
	var ids []int64
	for _, vr := range vrs {
		ids = append(ids, vr.ID)
	}

	_, err := s.Table(sutVariantTable).In(idField, ids).Delete(&sutVariantRow{})
	return err
}

func listSUTVariants(s xorm.Interface, vrID int64) ([]project.Variant, error) {
	vrs, err := listSUTVariantRows(s, vrID)
	if err != nil {
		return nil, err
	}

	var vs []project.Variant
	for _, vr := range vrs {
		v := sutVariantFromRow(vr)
		vs = append(vs, v)
	}

	return vs, nil
}

func listSUTVariantRows(s xorm.Interface, vrID int64) ([]sutVariantRow, error) {
	var vrs []sutVariantRow
	err := s.Table(sutVariantTable).Asc(nameField).Find(&vrs, &sutVariantRow{VersionID: vrID})
	if err != nil {
		return nil, err
	}

	return vrs, nil
}

func lookupSUTVariantRowIDs(s xorm.Interface, vrID int64, names ...string) ([]int64, error) {
	var ids []int64
	err := s.Table(sutVariantTable).Distinct(idField).In(versionField, vrID).In(nameField, names).Find(&ids)
	if err != nil {
		return nil, err
	}

	return ids, nil
}

func getSUTVariantRowByID(s xorm.Interface, variantID int64) (sutVariantRow, bool, error) {
	var variantRow sutVariantRow
	ex, err := s.Table(sutVariantTable).ID(variantID).Get(&variantRow)
	if err != nil || !ex {
		return sutVariantRow{}, false, err
	}

	return variantRow, true, nil
}

func variantRowSetMinus(s1, s2 []sutVariantRow) []sutVariantRow {
	rs := make([]sutVariantRow, len(s1))
	copy(rs, s1)

	for _, rvr := range s2 {
		for i := range rs {
			if rvr.Name == rs[i].Name {
				rs = append(rs[:i], rs[i+1:]...)
				break
			}
		}
	}

	return rs
}

func variantRowCutset(s1, s2 []sutVariantRow) []sutVariantRow {
	var rs []sutVariantRow
	for _, vr1 := range s1 {
		for _, vr2 := range s2 {
			if vr1.Name == vr2.Name {
				rs = append(rs, vr1)
				break
			}
		}
	}

	return rs
}
func renameSUTVariant(s xorm.Interface, p *project.Project, version, oldVariantName, newVariantName string) error {
	projectID, ex, err := getProjectRowID(s, p.ID())
	if !ex {
		return fmt.Errorf("can't find Project")
	}
	if err != nil {
		return err
	}

	versionRowID, err := lookupSUTVersionRowID(s, projectID, version)

	if err != nil {
		return err
	}

	rowID, err := lookupSUTVariantRowIDs(s, versionRowID, oldVariantName)
	if err != nil {
		return err
	}
	if len(rowID) == 0 {
		return fmt.Errorf("can't find Variant: " + oldVariantName)
	}

	ncr := sutVariantRow{ID: rowID[0], VersionID: versionRowID, Name: newVariantName}
	return updateSUTVariantRow(s, &ncr)
}
