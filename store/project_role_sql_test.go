// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package store

import (
	"testing"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
)

func TestRoleRowByNameForProject(t *testing.T) {
	prjEngine := ProjectsSQL{emptyEngine(t)}
	userEngine := UsersSQL{prjEngine.e}

	owner := id.NewActorID("testOwner")
	userEngine.Add(&user.PasswordUser{user.User{"testOwner", "testOwner", "a@b.c", time.Now(), false, "", true, false, 2, "", false}, "abc"})

	name1 := "Project-1"
	proj1 := project.NewProject(name1, owner, "", visibility.Public)
	prjEngine.Add(&proj1)
	proj1RowID, _, _ := getProjectRowID(prjEngine.e, proj1.ID())
	role1Row, _, _ := getRoleRowByNameForProject(prjEngine.e, "Supervisor", proj1RowID)

	name2 := "Project-2"
	proj2 := project.NewProject(name2, owner, "", visibility.Public)
	prjEngine.Add(&proj2)
	proj2RowID, _, _ := getProjectRowID(prjEngine.e, proj2.ID())
	role2Row, _, _ := getRoleRowByNameForProject(prjEngine.e, "Supervisor", proj2RowID)

	if role1Row.ID == role2Row.ID {
		t.Errorf("The owner of Project-2 has the role id associated with Project-1")
	}
}
